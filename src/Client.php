<?php

namespace Nolikein\BetterLaravelMattermostLogger;

use GuzzleHttp\Client as GuzzleClient;
use ThibaudDauce\Mattermost\MattermostException;
use ThibaudDauce\Mattermost\Message;

class Client
{
    /**
     * Guzzle HTTP Client.
     */
    public GuzzleClient $guzzle;

    /**
     * Default webhook URL.
     */
    public ?string $webhook;

    public function __construct(GuzzleClient $guzzle, ?string $webhook = null)
    {
        $this->guzzle = $guzzle;
        $this->webhook = $webhook;
    }

    public function send(Message $message, ?string $webhook = null): void
    {
        if (is_null($webhook) and is_null($this->webhook)) {
            throw new MattermostException('No default webhook configured. Please put a webhook URL as a second parameter of the constructor or of the `send` function.');
        }

        if (is_null($webhook)) {
            $webhook = $this->webhook;
        }

        $this->guzzle->post($webhook, [
            'json' => $message->toArray(),
            'Content-Type' => 'application/json',
        ]);
    }

    /**
     * @param array<string, mixed> $options
     */
    public function sendWithOptions(Message $message, ?string $webhook = null, array $options = []): void
    {
        if (is_null($webhook) and is_null($this->webhook)) {
            throw new MattermostException('No default webhook configured. Please put a webhook URL as a second parameter of the constructor or of the `send` function.');
        }

        if (is_null($webhook)) {
            $webhook = $this->webhook;
        }

        $options = array_merge([
            'json' => $message->toArray(),
            'Content-Type' => 'application/json',
        ], $options);

        $this->guzzle->post($webhook, $options);
    }
}
