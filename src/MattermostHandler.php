<?php

namespace Nolikein\BetterLaravelMattermostLogger;

use Closure;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Monolog\LogRecord;
use Nolikein\BetterLaravelMattermostLogger\Contracts\MattermostMessageInterface;
use ThibaudDauce\Mattermost\Message;

class MattermostHandler extends AbstractProcessingHandler
{
    protected Client $mattermost;

    /** @var array<string, mixed> */
    protected array $options = [];

    /** @var array<string, mixed> */
    protected array $httpOptions = [];
    public const ALLOWED_HTTP_OPTIONS = ['proxy'];

    protected Closure $message;

    /**
     * @param array<string, mixed> $options
     * @param Message $message A message that erase reccord. Usefull in tests.
     */
    public function __construct(Client $mattermost, array $options = [], ?Message $message = null)
    {
        $this->options = array_merge($this->defaults(), $options);
        $this->mattermost = $mattermost;

        foreach (self::ALLOWED_HTTP_OPTIONS as $key) {
            if (key_exists($key, $this->options)) {
                $this->httpOptions[$key] = $this->options[$key];
            }
        }

        // Pre-register a message from callback to allow Mock in tests.
        $this->message = null !== $message
            ? fn(): Message => $message
            : function (LogRecord $record): Message {
                // @codeCoverageIgnoreStart
                return App::makeWith(MattermostMessageInterface::class, [
                    'record' => $record,
                    'options' => $this->options,
                ]);
                // @codeCoverageIgnoreEnd
            };
    }

    /**
     * @param LogRecord $record
     */
    public function write(LogRecord $record): void
    {
        if ($record['level'] < $this->options['level']) {
            return;
        }

        $channels = Arr::wrap($this->options['channel']);

        $callback = $this->message;
        $message = $callback($record);

        foreach ($channels as $channel) {
            $this->options['channel'] = $channel;
            $this->mattermost->sendWithOptions(
                message: $message,
                webhook: $this->options['webhook'],
                options: $this->httpOptions
            );
        }
    }

    /**
     * Return default data
     *
     * @return array<string, mixed>
     */
    public function defaults(): array
    {
        return [
            'title' => 'An exception occured.',
            'webhook' => null,
            'channel' => ['town-square'],
            'icon_url' => null,
            'username' => 'Laravel Logs',
            'level' => Logger::INFO,
            'level_mention' => Logger::ERROR,
            'mentions' => ['@here'],
            'short_field_length' => 62,
            'max_attachment_length' => 6000,
        ];
    }
}
