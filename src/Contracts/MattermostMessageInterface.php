<?php

namespace Nolikein\BetterLaravelMattermostLogger\Contracts;

use Monolog\LogRecord;
use ThibaudDauce\Mattermost\Message as LowLevelMessage;
use Illuminate\Contracts\Config\Repository;
use ArrayAccess;

/**
 * @phpstan-import-type TRecord from \Nolikein\BetterLaravelMattermostLogger\Messages\HighLevelMessage
 * @phpstan-import-type TOptions from \Nolikein\BetterLaravelMattermostLogger\Messages\HighLevelMessage
 */
interface MattermostMessageInterface
{
    /**
     * Create a new message from a reccord and its options.
     * @param TRecord $record
     * @param TOptions $options
     * @param Repository $config Either inject from service container. Or fake.
     */
    public static function fromArrayAndOptions(
        array|ArrayAccess|LogRecord $record,
        array|ArrayAccess|LogRecord $options,
        ?Repository $config = null,
        ?HttpHistoryEntryInterface $entry = null
    ): LowLevelMessage;
}
