<?php

namespace Nolikein\BetterLaravelMattermostLogger\Contracts;

use Illuminate\Http\Request;

interface HttpHistoryEntryInterface
{
    /**
     * Create a new http history entry from a request and an exception.
     */
    public static function createFromRequest(Request $request, \Throwable $exception): self;

    /**
     * Convert the http history entry data into an array.
     * @phpstan-ignore missingType.return
     */
    public function toArray();
}
