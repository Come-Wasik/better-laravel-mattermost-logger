<?php

namespace Nolikein\BetterLaravelMattermostLogger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Nolikein\BetterLaravelMattermostLogger\Contracts\HttpHistoryEntryInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Auth\User as User;

/**
 * @property ?int $id
 * @property ?\Illuminate\Support\Carbon $created_at
 * @property ?\Illuminate\Support\Carbon $updated_at
 * @property string $app_environment
 * @property string $status_code
 * @property string $method
 * @property string $uri
 * @property string $action_performed
 * @property string $referrer
 * @property string $user_agent
 * @property string $ip
 * @property ?int $user_id
 * @property string $exception_class
 * @property string $exception_message
 */
class HttpHistoryEntry extends Model implements HttpHistoryEntryInterface
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'app_environment',
        'status_code',
        'method',
        'uri',
        'action_performed',
        'referrer',
        'user_agent',
        'ip',
        'user_id',
        'exception_class',
        'exception_message',
    ];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public static function createFromRequest(Request $request, ?\Throwable $exception, ?string $environment = null, ?int $userId = null): self
    {
        $statusCode = 200;
        $exceptionClass = '';
        $exceptionMessage = '';
        $environment ??= Config::get('app.env', 'local');
        $userId ??= Auth::id();

        // If an exception has been thrown, we should retrieve data concerning it
        if ($exception instanceof \Throwable) {
            $statusCode = 500;
            $exceptionClass = get_class($exception);
            $exceptionMessage = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            }
        }

        // Generate the model
        return new self([
            'app_environment' => $environment,
            'status_code' => $statusCode,
            'method' => $request->method(),
            'uri' => $request->getUri(),
            'action_performed' => $request->route() ? $request->route()->getAction('controller') : '',
            'referrer' => $request->header('referer') ?? '',
            'user_agent' => $request->header('user-agent') ?? '',
            'ip' => $request->ip(),
            'user_id' => $userId,
            'exception_class' => $exceptionClass,
            'exception_message' => $exceptionMessage,
        ]);
    }

    /**
     * Retrieve the user that executed the http request.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
