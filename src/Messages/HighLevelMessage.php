<?php

namespace Nolikein\BetterLaravelMattermostLogger\Messages;

use ArrayAccess;
use Illuminate\Support\Facades\URL;
use Monolog\LogRecord;
use ThibaudDauce\Mattermost\Attachment;
use ThibaudDauce\Mattermost\Message as LowLevelMessage;

/**
 * @phpstan-type TBaseRecord array{
 *      level_name: string,
 *      message: string,
 *      context: array{exception?: \Throwable},
 *      level: string,
 * }
 * @phpstan-type TBaseOptions array{
 *      channel: string,
 *      title: string,
 *      webhook: string,
 *      username: string,
 *      icon_url: string,
 *      mentions: array<int, string>,
 *      level: int,
 *      level_mention: int,
 *      max_attachment_length: ?int,
 *      short_field_length: ?int,
 * }
 *
 * @phpstan-type TRecord (TBaseRecord & array<string, mixed>)|\ArrayAccess<string, mixed>|LogRecord
 * @phpstan-type TOptions (TBaseOptions & array<string, mixed>)|\ArrayAccess<string, mixed>|LogRecord
 */
class HighLevelMessage
{
    /** @var TRecord */
    public array|ArrayAccess|LogRecord $record;

    /** @var TOptions */
    public array|ArrayAccess|LogRecord $options;
    public LowLevelMessage $message;
    public ?\Throwable $exception = null;

    /**
     * Construct a message
     * @param TRecord $record
     * @param TOptions $options
     */
    public function __construct(array|ArrayAccess|LogRecord $record, array|ArrayAccess|LogRecord $options)
    {
        $this->record = $record;
        $this->options = $options;
    }

    /**
     * Construct a message with base message
     * @param TRecord $record
     * @param TOptions $options
     */
    public static function fromArrayAndOptions(array|ArrayAccess|LogRecord $record, array|ArrayAccess|LogRecord $options): LowLevelMessage
    {
        $messageBuilder = new self($record, $options);

        $messageBuilder->exception = $messageBuilder->popException();

        $messageBuilder->createBaseMessage();
        $messageBuilder->addTitleText();
        $messageBuilder->addExceptionAttachment();
        $messageBuilder->addContextAttachment();

        return $messageBuilder->message;
    }

    /**
     * Set a default base message instance
     */
    public function createBaseMessage(): void
    {
        $this->message = (new LowLevelMessage)
            ->channel($this->options['channel'])
            ->username($this->options['username'])
            ->iconUrl($this->options['icon_url'] ? URL::to($this->options['icon_url']) : null);
    }


    /**
     * Add the title into the message instance.
     *
     * @see self::title()
     */
    public function addTitleText(): void
    {
        $this->message->text($this->title());
    }

    /**
     * Retrieve the title from the record data.
     */
    public function title(): string
    {
        $title = sprintf(
            '**[%s%s]** %s',
            $this->record['level_name'],
            $this->exception ? ' ' . get_class($this->exception) : '',
            $this->record['message']
        );

        if ($this->shouldMention()) {
            $title .= sprintf(' (ping %s)', $this->mentions());
        }

        return $title;
    }

    /**
     * Retrieve the mentions title from the options data.
     */
    public function mentions(): string
    {
        $mentions = array_map(function ($mention) {
            return $this->strStartsWith($mention, '@');
        }, $this->options['mentions']);

        return implode(', ', $mentions);
    }

    /**
     * Add the exception into the message instance.
     *
     * @see self::popException()
     */
    public function addExceptionAttachment(): void
    {
        if (!$this->exception) {
            return;
        }

        $this->attachment(function (Attachment $attachment) {
            $attachment->text(
                substr($this->exception->getTraceAsString(), 0, $this->options['max_attachment_length'])
            );
        });
    }

    /**
     * Retrieve the exception from the record data.
     */
    public function popException(): ?\Throwable
    {
        if (!isset($this->record['context']['exception'])) {
            return null;
        }

        $exception = $this->record['context']['exception'];
        unset($this->record['context']['exception']);

        return $exception;
    }

    /**
     * Add the context attachement into the message instance from the context.
     *
     * @see self::popException()
     */
    public function addContextAttachment(): void
    {
        if (!isset($this->record['context']) or empty($this->record['context'])) {
            return;
        }

        $this->attachment(function (Attachment $attachment) {
            foreach ($this->record['context'] as $key => $value) {
                $stringifyValue = is_string($value) ? $value : json_encode($value);
                $attachment->field($key, $stringifyValue, strlen($stringifyValue) < $this->options['short_field_length']);
            }
        });
    }

    /**
     * Determine if the message has the level to be mentionnable.
     */
    public function shouldMention(): bool
    {
        return $this->record['level'] >= $this->options['level_mention'];
    }

    /**
     * Add a message attachment.
     */
    public function attachment(\Closure $callback): void
    {
        $this->message->attachment(function (Attachment $attachment) use ($callback) {
            if ($this->shouldMention()) {
                $attachment->error();
            }

            $callback($attachment);
        });
    }

    /**
     * Checks if a string starts with.
     */
    public function strStartsWith(string $haystack, string $needle): bool
    {
        return strpos($haystack, $needle, 0) === 0;
    }
}
