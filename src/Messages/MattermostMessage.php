<?php

namespace Nolikein\BetterLaravelMattermostLogger\Messages;

use ArrayAccess;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Facades\App;
use Monolog\LogRecord;
use Nolikein\BetterLaravelMattermostLogger\Contracts\HttpHistoryEntryInterface;
use Nolikein\BetterLaravelMattermostLogger\Contracts\MattermostMessageInterface;
use ThibaudDauce\Mattermost\Message as LowLevelMessage;

/**
 * @phpstan-import-type TRecord from HighLevelMessage
 * @phpstan-import-type TOptions from HighLevelMessage
 */
class MattermostMessage extends HighLevelMessage implements MattermostMessageInterface
{
    protected Repository $config;
    protected HttpHistoryEntryInterface $historyEntry;

    /**
     * Construct from record and options.
     * @param TRecord $record
     * @param TOptions $options
     */
    public function __construct(
        array|ArrayAccess|LogRecord $record,
        array|ArrayAccess|LogRecord $options,
        ?Repository $config = null,
        ?HttpHistoryEntryInterface $entry = null
    ) {
        parent::__construct($record, $options);
        $this->config = $config ?? App::get('config');
        $this->historyEntry = $entry ?? App::make(HttpHistoryEntryInterface::class, [
            'exception' => $this->exception ?? null,
        ]);
    }

    /**
     * Construct a base message from record and options.
     *
     * @param TRecord $record
     * @param TOptions $options
     */
    public static function fromArrayAndOptions(
        array|ArrayAccess|LogRecord $record,
        array|ArrayAccess|LogRecord $options,
        ?Repository $config = null,
        ?HttpHistoryEntryInterface $entry = null
    ): LowLevelMessage {
        $messageBuilder = new self($record, $options, $config, $entry);

        $messageBuilder->exception = $messageBuilder->popException();

        $messageBuilder->createBaseMessage();
        $messageBuilder->addTitleText();
        $messageBuilder->addSubTitleText();
        $messageBuilder->addTableText();
        $messageBuilder->addExceptionAttachment();

        return $messageBuilder->message;
    }

    /**
     * Add the subtitle text
     */
    public function addSubTitleText(): void
    {
        $this->message->text($this->message->text . $this->subtitle());
    }

    /**
     * Add the table text
     * @return void
     */
    public function addTableText(): void
    {
        $this->message->text($this->message->text . $this->table());
    }

    /**
     * Retrieve the title content from options data.
     * @return string
     */
    public function title(): string
    {
        return $this->line('#### ' . $this->options['title']);
    }

    /**
     * Retrieve the subtitle content from options data.
     * @return string
     */
    public function subtitle(): string
    {
        return $this->line(parent::title() . $this->newline());
    }

    /**
     * Retrieve the title content from history entry data.
     * @return string
     */
    public function table(): string
    {
        $text = $this->line('| Header  | Content |');
        $text .= $this->line('| --- | --- |');
        $text .= $this->line('| Machine | ' . $this->getMachineEnv() . ' |');
        $text .= $this->line('| Environnement | ' . $this->getLaravelAppEnv() . ' |');

        foreach ($this->historyEntry->toArray() as $rowTitle => $rowValue) {
            $text .= $this->line('| ' . $rowTitle . ' | ' . $rowValue . ' |');
        }

        return $text;
    }

    /**
     * Retrive a content with a newline at the end.
     */
    public function line(string $content): string
    {
        return $content . "\n";
    }

    /**
     * Retrieve the newline symbol as string.
     */
    public function newline(): string
    {
        return "\n";
    }

    /**
     * Retrieve the laravel app environment
     */
    public function getLaravelAppEnv(): string
    {
        return $this->config->get('app.env', 'local');
    }

    /**
     * Retrieve the app machine environment.
     * You can define the "machine_env" key in your "config/app.php" file
     */
    public function getMachineEnv(): string
    {
        return $this->config->get('app.machine_env', 'developement');
    }
}
