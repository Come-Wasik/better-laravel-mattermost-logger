<?php

namespace Nolikein\BetterLaravelMattermostLogger;

use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Nolikein\BetterLaravelMattermostLogger\Commands\HttpHistoryMiddlewareMaker;
use Nolikein\BetterLaravelMattermostLogger\Commands\HttpHistoryMigrationMaker;
use Nolikein\BetterLaravelMattermostLogger\Commands\HttpHistoryModelMaker;
use Nolikein\BetterLaravelMattermostLogger\Contracts\HttpHistoryEntryInterface;
use Nolikein\BetterLaravelMattermostLogger\Contracts\MattermostMessageInterface;
use Nolikein\BetterLaravelMattermostLogger\Messages\MattermostMessage;

class MattermostServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Generate a new Mattermost message
        $this->app->bind(MattermostMessageInterface::class, function (Application $app, array $arguments) {
            return MattermostMessage::fromArrayAndOptions($arguments['record'], $arguments['options']);
        });

        // Generate a new Http History entry that is the same for the current request
        $this->app->singleton(HttpHistoryEntryInterface::class, function (Application $app, array $arguments) {
            return HttpHistoryEntry::createFromRequest(Request::capture(), $arguments['exception']);
        });

        // Add commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                HttpHistoryMigrationMaker::class,
                HttpHistoryModelMaker::class,
                HttpHistoryMiddlewareMaker::class,
            ]);
        }
    }
}
