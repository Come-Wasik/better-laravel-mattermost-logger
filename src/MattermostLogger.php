<?php

namespace Nolikein\BetterLaravelMattermostLogger;

use Monolog\Logger as Monolog;
use Nolikein\BetterLaravelMattermostLogger\MattermostHandler as CustomMattermostHandler;

class MattermostLogger
{
    public Client $mattermost;

    public function __construct(Client $mattermost)
    {
        $this->mattermost = $mattermost;
    }

    /**
     * Create a custom Monolog instance.
     *
     * @param array<string, string> $config
     */
    public function __invoke(array $config): Monolog
    {
        return new Monolog('mattermost', handlers: [new CustomMattermostHandler($this->mattermost, $config)]);
    }
}
