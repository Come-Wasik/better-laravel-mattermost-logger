<?php

namespace Nolikein\BetterLaravelMattermostLogger\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class HttpHistoryMigrationMaker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:http-history-entry-migration {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new mattermost logger model class';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tableName = (string) $this->argument('table');

        $stub = file_get_contents($this->getStub('migrations.stub'));
        $stub = str_replace('{{ table }}', $tableName, $stub);

        file_put_contents($this->getOutputPath($tableName), $stub);

        return Command::SUCCESS;
    }

    protected function getStub(string $name): string
    {
        return dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . ltrim($name, '/\\');
    }

    public function getOutputPath(string $table): string
    {
        return database_path('migrations' . DIRECTORY_SEPARATOR . $this->getDatePrefix() . '_' . Str::snake($table) . '.php');
    }

    protected function getDatePrefix(): string
    {
        return date('Y_m_d_His');
    }
}
