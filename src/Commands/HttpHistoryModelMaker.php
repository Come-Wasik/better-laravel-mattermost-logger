<?php

namespace Nolikein\BetterLaravelMattermostLogger\Commands;

use Illuminate\Console\Command;

class HttpHistoryModelMaker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:http-history-entry-model {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new mattermost logger model class';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $modelName = (string) $this->argument('model');

        $stub = file_get_contents($this->getStub('http-history-entry.model.stub'));
        $stub = str_replace('{{ modelname }}', $modelName, $stub);

        file_put_contents($this->getOutputPath($modelName), $stub);

        return Command::SUCCESS;
    }

    protected function getStub(string $name): string
    {
        return dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . ltrim($name, '/\\');
    }

    public function getOutputPath(string $modelName): string
    {
        return app_path('Models' . DIRECTORY_SEPARATOR . $modelName . '.php');
    }
}
