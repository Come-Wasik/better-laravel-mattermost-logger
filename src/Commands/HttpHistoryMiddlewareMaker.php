<?php

namespace Nolikein\BetterLaravelMattermostLogger\Commands;

use Illuminate\Console\Command;

class HttpHistoryMiddlewareMaker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:http-history-middleware {classname}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new middleware that store http history';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $className = (string) $this->argument('classname');

        $stub = file_get_contents($this->getStub('http-history-entry.middleware.stub'));
        $stub = str_replace('{{ classname }}', $className, $stub);

        file_put_contents($this->getOutputPath($className), $stub);

        return Command::SUCCESS;
    }

    protected function getStub(string $name): string
    {
        return dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . ltrim($name, '/\\');
    }

    public function getOutputPath(string $className): string
    {
        return app_path('Http' . DIRECTORY_SEPARATOR . 'Middleware' . DIRECTORY_SEPARATOR . $className . '.php');
    }
}
