<?php

use Illuminate\Contracts\Config\Repository;
use Nolikein\BetterLaravelMattermostLogger\Contracts\HttpHistoryEntryInterface;
use Nolikein\BetterLaravelMattermostLogger\Messages\MattermostMessage;
use ThibaudDauce\Mattermost\Message as LowLevelMessage;

it('can create a message', function (): void {

    /** @var \Mockery\MockInterface&Repository $config */
    $config = Mockery::mock(Repository::class);
    $config->shouldReceive('get')->with('app.env', 'local')->andReturn('local');
    $config->shouldReceive('get')->with('app.machine_env', 'developement')->andReturn('developement');

    /** @var \Mockery\MockInterface&HttpHistoryEntryInterface $entry */
    $entry = Mockery::mock(HttpHistoryEntryInterface::class);
    $entry->shouldReceive('toArray')->andReturn(['hello' => 'world']);

    $message = MattermostMessage::fromArrayAndOptions(
        record: [
            'level_name' => 'ERROR',
            'message' => 'world',
            'level' => 'ERROR',
        ],
        options: [
            'mentions' => ['@me'],
            'channel' => 'hello',
            'username' => 'hello',
            'icon_url' => null,
            'title' => 'Title',
            'level_mention' => 'ERROR',
        ],
        config: $config,
        entry: $entry
    );

    expect($message)->toBeInstanceOf(LowLevelMessage::class);
});
