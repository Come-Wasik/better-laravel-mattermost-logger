<?php

use Illuminate\Http\Request;
use Nolikein\BetterLaravelMattermostLogger\HttpHistoryEntry;
use Symfony\Component\HttpKernel\Exception\HttpException;

it('can create model without exception', function (): void {
    /** @var \Mockery\MockInterface&Request $request */
    $request = Mockery::mock(Request::class);
    $request->shouldReceive('method', 'getUri', 'route', 'header', 'ip')->andReturn('');

    $model = HttpHistoryEntry::createFromRequest(
        request: $request,
        exception: null,
        environment: 'production',
        userId: 0
    );

    expect($model)->toBeInstanceOf(HttpHistoryEntry::class);
});

it('can create model with exception', function (): void {
    /** @var \Mockery\MockInterface&Request $request */
    $request = Mockery::mock(Request::class);
    $request->shouldReceive('method', 'getUri', 'route', 'header', 'ip')->andReturn('');

    $model = HttpHistoryEntry::createFromRequest(
        request: $request,
        exception: new HttpException(500, 'test'),
        environment: 'production',
        userId: 0
    );

    expect($model)->toBeInstanceOf(HttpHistoryEntry::class);
});
