<?php

use Nolikein\BetterLaravelMattermostLogger\MattermostLogger;
use Monolog\Logger as Monolog;
use Nolikein\BetterLaravelMattermostLogger\Client;

it('can invoke logger', function (): void {
    /** @var MockInterface&Client $client */
    $client = Mockery::mock(Client::class);

    $logger = new MattermostLogger($client);
    $monolog = $logger([]);
    expect($monolog)->toBeInstanceOf(Monolog::class);
});
