<?php

use Monolog\LogRecord;
use Nolikein\BetterLaravelMattermostLogger\Client;
use Nolikein\BetterLaravelMattermostLogger\MattermostHandler;
use ThibaudDauce\Mattermost\Message;
use Mockery\MockInterface;

it('can write from handler', function (): void {
    /** @var MockInterface&Client $client */
    $client = Mockery::mock(Client::class);
    $client->shouldReceive('sendWithOptions')->andReturn(null);

    /** @var MockInterface&Client $record */
    $record = Mockery::mock(LogRecord::class);
    $record->shouldReceive('offsetGet')->with('level')->andReturn(9);
    $record->shouldReceive('offsetGet')->with('channel')->andReturn('test');

    /** @var \Mockery\MockInterface&Message $message */
    $message = Mockery::mock(Message::class);

    $handler = new MattermostHandler($client, [
        'proxy' => 'http://proxy.com',
        'level' => 1,
    ], $message);
    $handler->write($record);
})->throwsNoExceptions();

it('cannot write if level do not pass', function (): void {
    /** @var MockInterface&Client $client */
    $client = Mockery::mock(Client::class);
    $client->shouldReceive('sendWithOptions')->andReturn(null);

    /** @var MockInterface&Client $record */
    $record = Mockery::mock(LogRecord::class);
    $record->shouldReceive('offsetGet')->with('level')->andReturn(8);
    $record->shouldReceive('offsetGet')->with('channel')->andReturn('test');

    /** @var \Mockery\MockInterface&Message $message */
    $message = Mockery::mock(Message::class);

    $handler = new MattermostHandler($client, [
        'proxy' => 'http://proxy.com',
        'level' => 9,
    ], $message);
    $handler->write($record);
})->throwsNoExceptions();
