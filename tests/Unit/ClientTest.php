<?php

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Response;
use Nolikein\BetterLaravelMattermostLogger\Client;
use ThibaudDauce\Mattermost\MattermostException;
use ThibaudDauce\Mattermost\Message;

it('can mock send a message', function (): void {
    $webhook = 'http://fake-url.com';

    /** @var \Mockery\MockInterface&GuzzleClient $guzzle */
    $guzzle = Mockery::mock(GuzzleClient::class);
    $guzzle->shouldReceive('post')->andReturn(new Response());

    /** @var \Mockery\MockInterface&Message $message */
    $message = Mockery::mock(Message::class);
    $message->shouldReceive('toArray')->andReturn([]);

    $client = new Client($guzzle, $webhook);
    $client->send($message);
    expect($client)->toBeInstanceOf(Client::class);
});

it('can use webhook in send', function (): void {
    $webhook = 'http://fake-url.com';

    /** @var \Mockery\MockInterface&GuzzleClient $guzzle */
    $guzzle = Mockery::mock(GuzzleClient::class);
    $guzzle->shouldReceive('post')->andReturn(new Response());

    /** @var \Mockery\MockInterface&Message $message */
    $message = Mockery::mock(Message::class);
    $message->shouldReceive('toArray')->andReturn([]);

    $client = new Client($guzzle);
    $client->send($message, $webhook);
    expect($client)->toBeInstanceOf(Client::class);
});

it('can mock send a message with options', function (): void {
    $webhook = 'http://fake-url.com';

    /** @var \Mockery\MockInterface&GuzzleClient $guzzle */
    $guzzle = Mockery::mock(GuzzleClient::class);
    $guzzle->shouldReceive('post')->andReturn(new Response());

    /** @var \Mockery\MockInterface&Message $message */
    $message = Mockery::mock(Message::class);
    $message->shouldReceive('toArray')->andReturn([]);

    $client = new Client($guzzle, $webhook);
    $client->sendWithOptions($message, null, []);
    expect($client)->toBeInstanceOf(Client::class);
});

it('can use webhook in send with options', function (): void {
    $webhook = 'http://fake-url.com';

    /** @var \Mockery\MockInterface&GuzzleClient $guzzle */
    $guzzle = Mockery::mock(GuzzleClient::class);
    $guzzle->shouldReceive('post')->andReturn(new Response());

    /** @var \Mockery\MockInterface&Message $message */
    $message = Mockery::mock(Message::class);
    $message->shouldReceive('toArray')->andReturn([]);

    $client = new Client($guzzle);
    $client->sendWithOptions($message, $webhook, []);
    expect($client)->toBeInstanceOf(Client::class);
});

it('throws when webhook is not defined in send', function (): void {
    /** @var \Mockery\MockInterface&GuzzleClient $guzzle */
    $guzzle = Mockery::mock(GuzzleClient::class);
    $guzzle->shouldReceive('post')->andReturn(new Response());

    /** @var \Mockery\MockInterface&Message $message */
    $message = Mockery::mock(Message::class);
    $message->shouldReceive('toArray')->andReturn([]);

    $client = new Client($guzzle);
    $client->send($message);
    expect($client)->toBeInstanceOf(Client::class);
})->throws(MattermostException::class);

it('throws when webhook is not defined in send with options', function (): void {
    /** @var \Mockery\MockInterface&GuzzleClient $guzzle */
    $guzzle = Mockery::mock(GuzzleClient::class);
    $guzzle->shouldReceive('post')->andReturn(new Response());

    /** @var \Mockery\MockInterface&Message $message */
    $message = Mockery::mock(Message::class);
    $message->shouldReceive('toArray')->andReturn([]);

    $client = new Client($guzzle);
    $client->sendWithOptions($message);
    expect($client)->toBeInstanceOf(Client::class);
})->throws(MattermostException::class);
